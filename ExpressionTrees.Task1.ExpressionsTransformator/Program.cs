﻿/*
 * Create a class based on ExpressionVisitor, which makes expression tree transformation:
 * 1. converts expressions like <variable> + 1 to increment operations, <variable> - 1 - into decrement operations.
 * 2. changes parameter values in a lambda expression to constants, taking the following as transformation parameters:
 *    - source expression;
 *    - dictionary: <parameter name: value for replacement>
 * The results could be printed in console or checked via Debugger using any Visualizer.
 */
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace ExpressionTrees.Task1.ExpressionsTransformer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Expression Visitor for increment/decrement.");
            Console.WriteLine();

            Dictionary<string, object> replaceArgs = new Dictionary<string, object>
            {
                {"a", 8},
                {"b", 1},
                {"c", 4 },
                {"d", 2}
            };

            Expression<Func<int, int, int>> addition = (a, b) => a + b;

            Expression<Func<int, int>> sum = (a) => 1 + a + 3 + 4;

            Expression<Func<int, int>> multiple = a => (a + 1) * 2 + (a - 1) * 5;

            Expression<Func<int, int, int, int>> multiple2 =
                (a, b, c) => (a + b + 1 - c - 1) * 4 / (c + 1 + 1);

            Expression<Func<int, int, int, int, int>> multiple3 =
                (a, b, c, d) => (a + 1) * c + (b - 1 - d) / c;


            Process(addition, replaceArgs);

            Process(sum, replaceArgs);

            Process(multiple);

            Process(multiple2);

            Process(multiple3, replaceArgs);

            Console.ReadLine();
        }

        private static void Process(Expression expression, Dictionary<string, object> replaceArgs = null)
        {
            IncDecExpressionVisitor visitor = new IncDecExpressionVisitor();

            Console.WriteLine("Initial");
            Console.WriteLine(expression);

            Console.WriteLine("Replacement args");
            Console.WriteLine(GetDictionaryContents(replaceArgs));

            Expression result = visitor.Translate(expression, replaceArgs);

            Console.WriteLine("Translated");
            Console.WriteLine(result);
            Console.WriteLine();
        }

        private static string GetDictionaryContents(Dictionary<string, object> dictionary)
        {
            if (dictionary == null)
            {
                return string.Empty;
            }
            var sb = new StringBuilder();
            foreach (KeyValuePair<string, object> item in dictionary)
            {
                sb.AppendLine(string.Format("{0}: {1}", item.Key, item.Value));
            }
            return sb.ToString();
        }
    }
}
