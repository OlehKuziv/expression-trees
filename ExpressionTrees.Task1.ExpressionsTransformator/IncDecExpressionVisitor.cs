﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ExpressionTrees.Task1.ExpressionsTransformer
{
    public class IncDecExpressionVisitor : ExpressionVisitor
    {
        private Dictionary<string, object> parametersToReplace;

        public Expression Translate(Expression exp, Dictionary<string, object> parametersToReplace)
        {
            this.parametersToReplace = parametersToReplace ?? new Dictionary<string, object>();
            return Visit(exp);
        }

        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            // Leave all parameters alone except the one we want to replace.
            var parameters = node.Parameters
                .Where(p => !parametersToReplace.ContainsKey(p.Name)).ToArray();

            return Expression.Lambda(Visit(node.Body), parameters);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (parametersToReplace.ContainsKey(node.Name))
            {
                ConstantExpression constantExpression = Expression.Constant(parametersToReplace[node.Name]);
                return constantExpression;
            }
            // Replace the source with the target, visit other params as usual.
            return base.VisitParameter(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            if (node.NodeType == ExpressionType.Subtract)
            {
                bool mustDecrement = IsEqualToOne(node.Right);
                if (mustDecrement)
                {
                    return HandleDecrement(node.Left);
                }
                return base.VisitBinary(node);
            }
            if (node.NodeType == ExpressionType.Add)
            {
                if (IsEqualToOne(node.Left))
                {
                    return HandleIncrement(node.Right);
                }
                else if (IsEqualToOne(node.Right))
                {
                    return HandleIncrement(node.Left);
                }
                return base.VisitBinary(node);
            }
            return base.VisitBinary(node);
        }

        private bool IsEqualToOne(Expression node)
        {
            if (node.NodeType == ExpressionType.Constant && typeof(int).IsAssignableFrom(node.Type))
            {
                object constantValue = ((ConstantExpression)node).Value;
                int value = Convert.ToInt32(constantValue);
                if (value == 1)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        private Expression HandleDecrement(Expression expression)
        {
            // If you dont want to replace the variable in case it is declared in dictionary, then dont call visit here. Simply return decrement of original expression
            Expression finalExpressionWithReplacements = Visit(expression);
            return Expression.Decrement(finalExpressionWithReplacements);
        }

        private Expression HandleIncrement(Expression expression)
        {
            // If you dont want to replace the variable in case it is declared in dictionary, then dont call visit here. Simply return increment of original expression
            Expression finalExpressionWithReplacements = Visit(expression);
            return Expression.Increment(finalExpressionWithReplacements);
        }
    }
}
